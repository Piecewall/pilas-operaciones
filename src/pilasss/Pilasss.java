/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pilasss;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Orlando
 */
public class Pilasss {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Primer ejercicio");
        Pilasss.pi1();
        System.out.println("");
        System.out.println("Segundo ejercicio");
        Pilasss.pi2();
        System.out.println("");
        Scanner entrada = new Scanner(System.in);
        String respuesta = "S";
        String cadena="";
        System.out.println("Tercer ejercicio");
        while (respuesta.equalsIgnoreCase("S")){
            System.out.println ("¿Qué cadena desea analizar?");
            cadena=entrada.nextLine();
            String textoPorPantalla="";
            char[] array = {'a','e','i','o','u'};
            for (int i=0; i<array.length; i++) {
                switch (analizarVocal(cadena,array[i])){
                    case 1: textoPorPantalla=textoPorPantalla+"\nEl número de letras "+array[i]+" es par. "; break;
                    case -1: textoPorPantalla=textoPorPantalla+"\nEl número de letras "+array[i]+" es impar. "; break;
                    case 0: textoPorPantalla=textoPorPantalla+"\nEl número de letras "+array[i]+" es cero. "; break;
                }
            }
                System.out.println (textoPorPantalla);
                System.out.print ("¿Desea analizar otra cadena? (S/N) ");
                respuesta = entrada.nextLine();           
        }
        
    }
    public static void pi1(){
    String cadena = "(123*4)+((12-1)*(1+2))";
        Stack pila = new Stack();
        boolean flag = true;
        char[] acad = cadena.toCharArray();
        for(char c: acad){
        if(c =='('){
        pila.push(c);
        System.out.println("Insertando = "+c);
        }
        if(c == ')'){
        if(!pila.empty()){
        pila.pop();
        System.out.println("Extrayendo = "+c);
        } else {flag = false;}
        }
        }
        System.out.println(pila.size());
        System.out.println(flag);
        if(pila.size()==0 && flag){System.out.println("Expresión valida");} else
        {System.out.println("Expresión invalida");}
    
    }
    public static void pi2(){
    String cadena = "<b><i>Hola ISC</i></b>";
        Stack pila = new Stack();
        boolean flag = true;
        char[] acad = cadena.toCharArray();
        for(char c: acad){
        if(c =='<'){
        pila.push(c);
        System.out.println("Insertando = "+c);
        }
        if(c == '>'){
        if(!pila.empty()){
        pila.pop();
        System.out.println("Extrayendo = "+c);
        } else {flag = false;}
        }
        }
        System.out.println(pila.size());
        System.out.println(flag);
        if(pila.size()==0 && flag){System.out.println("Expresión valida");} else
        {System.out.println("Expresión invalida");}
    
    
    }
    public static int analizarVocal (String cadena, char vocalParaAnalizar) {
        Stack<String> pila = new Stack<String>(); char v=vocalParaAnalizar; String vocal=String.valueOf(vocalParaAnalizar);
        int i=0; int auxiliar=0;
        while (i<cadena.length()) {
            if (Character.toLowerCase(cadena.charAt(i))==v&&pila.empty()) {pila.push(vocal); auxiliar++;}
            else if (Character.toLowerCase(cadena.charAt(i))==v&&!pila.empty()) {pila.pop();}
            i++;
        }
        if (auxiliar==0) {return 0;} else { if (pila.empty()) {return 1;} else {return -1;}}     
    }  
    
}
